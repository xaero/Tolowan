-- phpMyAdmin SQL Dump
-- version 4.4.15.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2016-12-15 23:36:24
-- 服务器版本： 5.7.11-log
-- PHP Version: 7.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tolowan`
--

-- --------------------------------------------------------

--
-- 表的结构 `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL,
  `nid` int(11) NOT NULL,
  `pid` int(11) DEFAULT '0',
  `uid` int(11) DEFAULT '0',
  `created` int(11) NOT NULL,
  `changed` int(11) NOT NULL,
  `love` int(10) DEFAULT '0',
  `body` text NOT NULL,
  `contentModel` varchar(32) DEFAULT 'comment'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `comment`
--

INSERT INTO `comment` (`id`, `nid`, `pid`, `uid`, `created`, `changed`, `love`, `body`, `contentModel`) VALUES
(1, 1, 0, 1, 1481801274, 1481801274, 0, '<p>你好，先生！@</p>\r\n', 'comment');

-- --------------------------------------------------------

--
-- 表的结构 `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `state` int(1) DEFAULT '1',
  `content_type` varchar(10) NOT NULL,
  `access` int(1) DEFAULT '1',
  `md5` varchar(64) DEFAULT '',
  `name` varchar(80) DEFAULT '',
  `description` text,
  `path` varchar(60) NOT NULL,
  `changed` int(11) NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `meta`
--

CREATE TABLE IF NOT EXISTS `meta` (
  `id` varchar(32) NOT NULL,
  `data` varchar(225) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `meta`
--

INSERT INTO `meta` (`id`, `data`) VALUES
('node_browse_1', '8');

-- --------------------------------------------------------

--
-- 表的结构 `node`
--

CREATE TABLE IF NOT EXISTS `node` (
  `id` int(11) NOT NULL,
  `state` int(1) DEFAULT '1' COMMENT '文章是否被标记为删除状态',
  `contentModel` varchar(32) NOT NULL COMMENT '文章类型',
  `created` int(11) NOT NULL COMMENT '文章创建时间',
  `changed` int(11) NOT NULL COMMENT '文章最新更改时间',
  `uid` int(11) DEFAULT '0' COMMENT '文章作者',
  `comment` int(1) DEFAULT '0' COMMENT '文章是否开启评论',
  `top` int(1) DEFAULT '0' COMMENT '文章被标记为置顶，1为置顶，0为不制定',
  `essence` int(1) DEFAULT '0' COMMENT '标记为精华，1为精华，0则不是',
  `hot` tinyint(1) DEFAULT '0',
  `love` int(11) DEFAULT '0',
  `comment_num` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `node`
--

INSERT INTO `node` (`id`, `state`, `contentModel`, `created`, `changed`, `uid`, `comment`, `top`, `essence`, `hot`, `love`, `comment_num`) VALUES
(1, 1, 'article', 1481801237, 1481801237, 1, 1, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `node_book`
--

CREATE TABLE IF NOT EXISTS `node_book` (
  `id` int(11) NOT NULL,
  `nid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `pid` int(11) DEFAULT '0',
  `weight` int(11) DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `created` int(11) NOT NULL,
  `changed` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `node_field_body`
--

CREATE TABLE IF NOT EXISTS `node_field_body` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` text NOT NULL,
  `full_text` text,
  `toc` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `node_field_body`
--

INSERT INTO `node_field_body` (`id`, `eid`, `value`, `full_text`, `toc`) VALUES
(1, 1, '<p>欢迎使用Tolowan</p>\n', 'tolowan \\u4f7f\\u7528 \\u6b22\\u8fce ', '[]');

-- --------------------------------------------------------

--
-- 表的结构 `node_field_cat`
--

CREATE TABLE IF NOT EXISTS `node_field_cat` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `node_field_description`
--

CREATE TABLE IF NOT EXISTS `node_field_description` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `node_field_description`
--

INSERT INTO `node_field_description` (`id`, `eid`, `value`) VALUES
(1, 1, '欢迎使用Tolowan');

-- --------------------------------------------------------

--
-- 表的结构 `node_field_download`
--

CREATE TABLE IF NOT EXISTS `node_field_download` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `node_field_images`
--

CREATE TABLE IF NOT EXISTS `node_field_images` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `node_field_images`
--

INSERT INTO `node_field_images` (`id`, `eid`, `value`) VALUES
(1, 1, '');

-- --------------------------------------------------------

--
-- 表的结构 `node_field_keywords`
--

CREATE TABLE IF NOT EXISTS `node_field_keywords` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `node_field_keywords`
--

INSERT INTO `node_field_keywords` (`id`, `eid`, `value`) VALUES
(1, 1, '欢迎使用Tolowan');

-- --------------------------------------------------------

--
-- 表的结构 `node_field_tags`
--

CREATE TABLE IF NOT EXISTS `node_field_tags` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `node_field_title`
--

CREATE TABLE IF NOT EXISTS `node_field_title` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `node_field_title`
--

INSERT INTO `node_field_title` (`id`, `eid`, `value`) VALUES
(1, 1, '欢迎使用Tolowan');

-- --------------------------------------------------------

--
-- 表的结构 `queue`
--

CREATE TABLE IF NOT EXISTS `queue` (
  `id` int(11) NOT NULL,
  `type` int(1) NOT NULL,
  `data` text NOT NULL,
  `runtime` int(11) DEFAULT NULL,
  `state` int(1) NOT NULL,
  `error` text,
  `weight` int(11) DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `term`
--

CREATE TABLE IF NOT EXISTS `term` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `description` varchar(400) DEFAULT NULL,
  `parent` int(11) DEFAULT '0',
  `widget` int(11) DEFAULT '10',
  `other` text,
  `contentModel` varchar(32) NOT NULL,
  `attach` text,
  `changed` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `keywords` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `name` varchar(11) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `phone` bigint(11) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `created` int(11) NOT NULL,
  `active` int(1) DEFAULT '0',
  `email_validate` tinyint(1) DEFAULT '0',
  `phone_validate` tinyint(1) DEFAULT '0',
  `changed` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `phone`, `password`, `created`, `active`, `email_validate`, `phone_validate`, `changed`) VALUES
(1, '管蠡园', 'admin@admin.com', 888888, '$2y$08$bS9XN2JIa3JiWldIbG1NUeSbzU90l2uHrjM4ON2hmuOGuaili8iIG', 1467525643, 1, 0, 0, 1480169564);

-- --------------------------------------------------------

--
-- 表的结构 `user_field_blog`
--

CREATE TABLE IF NOT EXISTS `user_field_blog` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_field_description`
--

CREATE TABLE IF NOT EXISTS `user_field_description` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_field_face`
--

CREATE TABLE IF NOT EXISTS `user_field_face` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user_field_face`
--

INSERT INTO `user_field_face` (`id`, `eid`, `value`) VALUES
(1, 1, '/file/user/face/0/1.png');

-- --------------------------------------------------------

--
-- 表的结构 `user_field_gold`
--

CREATE TABLE IF NOT EXISTS `user_field_gold` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user_field_gold`
--

INSERT INTO `user_field_gold` (`id`, `eid`, `value`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `user_field_profession`
--

CREATE TABLE IF NOT EXISTS `user_field_profession` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_field_roles`
--

CREATE TABLE IF NOT EXISTS `user_field_roles` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` varchar(30) NOT NULL,
  `created` int(11) NOT NULL,
  `state` int(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user_field_roles`
--

INSERT INTO `user_field_roles` (`id`, `eid`, `value`, `created`, `state`) VALUES
(14, 1, 'admin', 1469909144, 1),
(16, 1, 'user', 1469909185, 1);

-- --------------------------------------------------------

--
-- 表的结构 `user_field_score`
--

CREATE TABLE IF NOT EXISTS `user_field_score` (
  `id` int(11) NOT NULL,
  `eid` int(11) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user_field_score`
--

INSERT INTO `user_field_score` (`id`, `eid`, `value`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `created` int(11) NOT NULL,
  `notice` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nid` (`nid`),
  ADD KEY `pid` (`pid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `changed` (`changed`),
  ADD KEY `contentModel` (`contentModel`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `content_type` (`content_type`),
  ADD KEY `created` (`created`);

--
-- Indexes for table `meta`
--
ALTER TABLE `meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `node`
--
ALTER TABLE `node`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contentModel` (`contentModel`),
  ADD KEY `created` (`created`),
  ADD KEY `changed` (`changed`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `node_book`
--
ALTER TABLE `node_book`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nid` (`nid`),
  ADD KEY `bid` (`bid`),
  ADD KEY `pid` (`pid`),
  ADD KEY `weight` (`weight`);

--
-- Indexes for table `node_field_body`
--
ALTER TABLE `node_field_body`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eid` (`eid`),
  ADD FULLTEXT KEY `full_text` (`full_text`);

--
-- Indexes for table `node_field_cat`
--
ALTER TABLE `node_field_cat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eid` (`eid`),
  ADD KEY `value` (`value`);

--
-- Indexes for table `node_field_description`
--
ALTER TABLE `node_field_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eid` (`eid`);

--
-- Indexes for table `node_field_download`
--
ALTER TABLE `node_field_download`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eid` (`eid`);

--
-- Indexes for table `node_field_images`
--
ALTER TABLE `node_field_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eid` (`eid`);

--
-- Indexes for table `node_field_keywords`
--
ALTER TABLE `node_field_keywords`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eid` (`eid`);

--
-- Indexes for table `node_field_tags`
--
ALTER TABLE `node_field_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eid` (`eid`),
  ADD KEY `value` (`value`);

--
-- Indexes for table `node_field_title`
--
ALTER TABLE `node_field_title`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eid` (`eid`);

--
-- Indexes for table `queue`
--
ALTER TABLE `queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `term`
--
ALTER TABLE `term`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`,`contentModel`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `user_field_blog`
--
ALTER TABLE `user_field_blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eid` (`eid`) USING BTREE;

--
-- Indexes for table `user_field_description`
--
ALTER TABLE `user_field_description`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eid` (`eid`) USING BTREE;

--
-- Indexes for table `user_field_face`
--
ALTER TABLE `user_field_face`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eid` (`eid`) USING BTREE;

--
-- Indexes for table `user_field_gold`
--
ALTER TABLE `user_field_gold`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eid` (`eid`) USING BTREE;

--
-- Indexes for table `user_field_profession`
--
ALTER TABLE `user_field_profession`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eid` (`eid`) USING BTREE;

--
-- Indexes for table `user_field_roles`
--
ALTER TABLE `user_field_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eid` (`eid`),
  ADD KEY `value` (`value`);

--
-- Indexes for table `user_field_score`
--
ALTER TABLE `user_field_score`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `eid` (`eid`) USING BTREE;

--
-- Indexes for table `user_log`
--
ALTER TABLE `user_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`,`type`,`created`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `node`
--
ALTER TABLE `node`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `node_book`
--
ALTER TABLE `node_book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `node_field_body`
--
ALTER TABLE `node_field_body`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `node_field_cat`
--
ALTER TABLE `node_field_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `node_field_description`
--
ALTER TABLE `node_field_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `node_field_download`
--
ALTER TABLE `node_field_download`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `node_field_images`
--
ALTER TABLE `node_field_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `node_field_keywords`
--
ALTER TABLE `node_field_keywords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `node_field_tags`
--
ALTER TABLE `node_field_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `node_field_title`
--
ALTER TABLE `node_field_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `queue`
--
ALTER TABLE `queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `term`
--
ALTER TABLE `term`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_field_blog`
--
ALTER TABLE `user_field_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_field_description`
--
ALTER TABLE `user_field_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_field_face`
--
ALTER TABLE `user_field_face`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_field_gold`
--
ALTER TABLE `user_field_gold`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_field_profession`
--
ALTER TABLE `user_field_profession`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_field_roles`
--
ALTER TABLE `user_field_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `user_field_score`
--
ALTER TABLE `user_field_score`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_log`
--
ALTER TABLE `user_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
